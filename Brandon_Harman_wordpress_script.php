// Include custom jQuery by adding this snippet in the head tags in the header of Wordpress
function shapeSpace_include_custom_jquery() {
// Remove wordpress' core jquery
	wp_deregister_script('jquery');
// Add in newest version of jquery
	wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.2.1.js', array(), null, true);

}
// Add the action hook for wordpress to use
add_action('wp_enqueue_scripts', 'shapeSpace_include_custom_jquery');