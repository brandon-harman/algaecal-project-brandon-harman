
// -------- Video controls -------- //
window._wq = window._wq || [];

// Target the video ID with first 3 letters
_wq.push({ id: "cec", onReady: function(video) {

// On video click remove the default image and start the video
	$(".video-image").click(function(){
		// Change the z-index of the default video image to 0 to fall beind the video when playing
		$('.video-image').css('z-index','0');
		video.play();
	});

  // bind a action to a specific time in the video
	video.bind("timechange", function(t) {
		//Check to see if the video has ended and add default image back in
	  	if (t > 1044) {
	  		// Change the z-index of the image to 10 to add default image back in overlaying video
	    	$('.video-image').css('z-index','10');
	  }
	});

  // bind a action to a specific time in the video
	video.bind("timechange", function(t) {
		//check to see if the time on the video has reach 2:13
	  	if (t > 133) {
	  		// Add active class to the products to display them
	    	$('.all-product-boxes').addClass('active').show();

	    return video.unbind;
	  }
	});	

}});




// -------- Show/hide store phone # during business hours only

// Set the current date, hours and convert the date to a string
var date = new Date();
var hours = date.getHours();
var newDate = date.toString();

// Creating 2 arrays for weekdays and weekends to check day of the week against to determine open hours
var weekdays = ['Mon','Tue','Wed','Thu','Fri'];
var weekends = ['Sat','Sun'];

// Remove all other parts of newDate besides the day of the week abbrivation
var result = newDate.substring(0, 3);

// Check to see if the day of the week is a weekday
if (weekdays.includes(result)) {

		//Check if the current hour is between the business hours of 6am-4pm and show contact copy
  		if(hours >= 6 && hours < 16 ){
		    $(".tap-to-talk").show();
		}
		
		//Must be a weekend. so check if the current hour is between the business hours of 8am-2pm and show contact copy
		} else if(hours >= 8 && hours < 2 ){
		    $(".tap-to-talk").show();
		}





	// API calls to populate phone number and modal
$(function() {

	$.ajax({

		type: 'GET',
		url: 'https://www.algaecal.com/wp-json/acf/v2/options',

		success: function(data) {
			$.each(data, function (i, item) {
				// Get phone number from the API and populate the appropriate area
				var phoneNumber = (item.default_phone_number);
				$('.phone-number').html(phoneNumber);

				// Get 7 Year Guarantee Long Version from the API and populate the modal
				var sevenYear = (item["7yr_full_copy"]);
				$('.guarantee-long-version').html(sevenYear);			

			});

		}

	});

});


// When clicking to open the modal popup add the responsive video to the bottom
$(".guarantee").on("click", function() { 
    $('#guarantee').find('.embed-responsive').prepend('<iframe style="width:100%" src="https://algaecal.wistia.com/medias/oxo4ak3z2d?embedType=async&videoWidth=800&videoFoam=true"></iframe>');

});
