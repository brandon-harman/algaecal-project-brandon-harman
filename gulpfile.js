// Import gulp node package
var gulp = require('gulp'),
	sass = require('gulp-sass')

function errorLog (error) {
	console.error.bind(console);
	this.emit('end');
}

// Gulp talk to console.log
gulp.task('styles', function(){

	gulp.src('scss/*.scss')
		.pipe(sass({
			outputStyle: 'compressed'
		}))
		// on event console log error
		.on('error', errorLog)
		.pipe(gulp.dest('css/'));

})

// Gulp task to watch SCSS changes and complile them in style.css
gulp.task('watch', function () {

	gulp.watch('scss/*.scss', ['styles']);

})

// Default task for 'gulp' setup with an array of tasks
gulp.task('default', ['styles', 'watch',]);